﻿using DocumentFormat.OpenXml.Wordprocessing;
using Npgsql;
using System.Data;
using System.Data.SqlClient;

namespace DZ2_OtusEdu
{
    class Program
    {
        const string connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=123;Database=Otus";

        public static void Main(string[] args)
        {
            //VewRowsTables();
            InsertRowToTable();
        }

        static void VewRowsTables()
        {
            IPrintData<StudentsModel> studentsData = new PrintData<StudentsModel>();
            IPrintData<CoursesModel> coursesData = new PrintData<CoursesModel>();
            IPrintData<GroupsEducationsModel> GeData = new PrintData<GroupsEducationsModel>();

            List<StudentsModel> dataModelStudents = GetData("students").AsEnumerable().Select(x =>
                new StudentsModel
                {
                    Id = x.Field<long>("id"),
                    NumberStudent = x.Field<string>("numberstudent"),
                    Surname = x.Field<string>("surname"),
                    Name = x.Field<string>("name"),
                    Patronymic = x.Field<string>("patronymic"),
                    DateBirth = x.Field<DateTime>("datebirth"),
                    Email = x.Field<string>("email")
                }).ToList();

            List<CoursesModel> dataModelCourses = GetData("courses").AsEnumerable().Select(x =>
                new CoursesModel
                {
                    Id = x.Field<long>("id"),
                    Namecourse = x.Field<string>("namecourse"),
                    Coursecode = x.Field<int>("coursecode")
                }).ToList();

            List<GroupsEducationsModel> dataModelGEs = GetData("groupseducations").AsEnumerable().Select(x =>
                new GroupsEducationsModel
                {
                    Id = x.Field<long>("id"),
                    StudentId = x.Field<string>("studentId"),
                    CourseId = x.Field<string>("courseId"),
                    DateStart = x.Field<DateTime>("dateStart"),
                    DateEnd = x.Field<DateTime>("dateend")
                }).ToList();


            studentsData.PrintTable(dataModelStudents);
            coursesData.PrintTable(dataModelCourses);
            GeData.PrintTable(dataModelGEs);
        }

        static DataTable GetData(string tableName)
        {
            var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            string sql = string.Format("select * from Otus.{0}", tableName);
            using var cmd = new NpgsqlCommand(sql, connection);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(rdr);

            connection.Close();

            return dt;
        }

        static void InsertRowToTable()
        {
            Console.WriteLine("Выберите таблицу для расширения: 1. Студенты (Students), 2. Курсы (Courses), 3. Потоки студентов на курсах (GroupsEducations)");
            Console.WriteLine("Введите только номер таблицы");
            int numTable = Convert.ToInt32(Console.ReadLine());

            string insertSqlQuery = "";

            switch (numTable)
            {
                case 1:
                    insertSqlQuery = GetInsertSqlQueryStudent();
                    break;
                case 2:
                    insertSqlQuery = GetInsertSqlQueryCourse();
                    break;
                case 3:
                    insertSqlQuery = GetInsertSqlQueryGrEd();
                    break;
            }

            InsertRow(insertSqlQuery);
        }

        static void InsertRow(string insertIntoData)
        {
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            using var cmd = new NpgsqlCommand(insertIntoData, connection);
            var cnt = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine("Данные успешно добавлены");

            connection.Close();
        }

        static string GetInsertSqlQueryStudent()
        {
            Console.WriteLine("Введите значение NumberStudent: ");
            string insertedNumberStudent = Console.ReadLine();
            Console.WriteLine("Введите значение Surname: ");
            string insertedSurname = Console.ReadLine();
            Console.WriteLine("Введите значение Name: ");
            string insertedName = Console.ReadLine();
            Console.WriteLine("Введите значение Patronymic: ");
            string insertedPatronymic = Console.ReadLine();
            Console.WriteLine("Введите значение DateBirth в формате yyyy-mm-dd: ");
            string insertedDateBirth = Console.ReadLine();
            Console.WriteLine("Введите значение Email: ");
            string insertedEmail = Console.ReadLine();

            var sql = string.Format(@"insert into otus.students(Id, NumberStudent, Surname, Name, Patronymic, DateBirth, Email)
values(nextval('otus.students_id_seq'), '{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",
insertedNumberStudent,
insertedSurname,
insertedName,
insertedPatronymic,
insertedDateBirth,
insertedEmail);

            return sql;
        }

        static string GetInsertSqlQueryCourse()
        {
            Console.WriteLine("Введите значение namecourse: ");
            string insertednamecourse = Console.ReadLine();
            Console.WriteLine("Введите значение coursecode: ");
            string insertedcoursecode = Console.ReadLine();

            var sql = string.Format(@"insert into otus.courses(Id, namecourse, coursecode)
values(nextval('otus.courses_id_seq'), '{0}', '{1}')",
insertednamecourse,
insertedcoursecode);

            return sql;
        }

        static string GetInsertSqlQueryGrEd()
        {
            Console.WriteLine("Введите значение studentId: ");
            string insertedstudentId = Console.ReadLine();
            Console.WriteLine("Введите значение courseId: ");
            string insertedcourseId = Console.ReadLine();
            Console.WriteLine("Введите значение dateStart в формате yyyy-mm-dd: ");
            string inserteddateStart = Console.ReadLine();
            Console.WriteLine("Введите значение dateend в формате yyyy-mm-dd: ");
            string inserteddateend = Console.ReadLine();

            var sql = string.Format(@"insert into otus.groupseducations(Id, studentId, courseId, dateStart, dateend)
values(nextval('otus.GroupsEducations_id_seq'), '{0}', '{1}', '{2}', '{3}')",
insertedstudentId,
insertedcourseId,
inserteddateStart,
inserteddateend);

            return sql;
        }

    }
}
