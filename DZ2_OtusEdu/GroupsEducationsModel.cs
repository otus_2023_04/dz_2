﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ2_OtusEdu
{
    internal class GroupsEducationsModel
    {
        public long Id { get; set; }
        public string StudentId { get; set; }
        public string CourseId { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
