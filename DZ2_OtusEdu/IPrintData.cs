﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ2_OtusEdu
{
    public interface IPrintData<TData>
    {
        void PrintTable(IEnumerable<TData> data);
    }
}
