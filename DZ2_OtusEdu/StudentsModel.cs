﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ2_OtusEdu
{
    internal class StudentsModel
    {
        public long Id { get; set; }
        public string NumberStudent { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateBirth { get; set; }
        public string Email { get; set; }
    }
}
