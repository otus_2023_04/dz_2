﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DZ2_OtusEdu
{
    public sealed class PrintData<TData> : IPrintData<TData>
    {
        private const int _tableWidth = 150;
        public void PrintTable(IEnumerable<TData> data)
        {
            try
            {
                PropertyInfo[] propertyInfos = typeof(TData).GetProperties();
                var propertyName = propertyInfos.Select(x => x.Name).ToArray();

                GetSepSimble();
                PrintRow(propertyName);
                GetSepSimble();

                foreach (var row in data)
                {
                    var values = propertyInfos.Select(x => x.GetValue(row, null).ToString()).ToArray();
                    PrintRow(values);
                }

                GetSepSimble();         
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GetSepSimble()
        {
            Console.WriteLine(new String('-', _tableWidth));
        }

        public static void PrintRow(params string[] columns)
        {
            int columnWidth = (_tableWidth - columns.Length) / columns.Length;
            const string columnSeperator = "|";

            string row = columns.Aggregate(columnSeperator, (seperator, columnText) => seperator + GetCenterAlignedText(columnText, columnWidth) + columnSeperator);

            Console.WriteLine(row);
        }

        private static string GetCenterAlignedText(string text, int columnWidth)
        {
            text = text.Length > columnWidth ? text.Substring(0, columnWidth - 3) + "_" : text;

            return string.IsNullOrEmpty(text)
                ? new string(' ', columnWidth)
                : text.PadRight(columnWidth - ((columnWidth - text.Length) / 2)).PadLeft(columnWidth);
        }
    }
}
