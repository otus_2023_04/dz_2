﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ2_OtusEdu
{
    internal class CoursesModel
    {
        public long Id { get; set; }
        public string Namecourse { get; set; }
        public int Coursecode { get; set; }
    }
}
